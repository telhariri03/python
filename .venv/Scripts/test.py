from flask import Flask
app = Flask(__name__)
app.config.from_object('config')


@app.route("/")
def hello():
    return "hello taha"

@app.route("/user/<username>")
def show(username):
    return "User {}".format(username)

@app.route("/post/<int:id_post>")
def num(id_post):
    return "post numero {}".format(id_post)

@app.route("/path/<path:more>")
def pa(more):
    return "more: {}".format(more)

if __name__=="__main__":
    app.run()