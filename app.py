from flask import Flask , render_template
from flask_pymongo import PyMongo


app = Flask(__name__ , template_folder="templates")
app.config.from_object('config')
app.config['MONGO_URI'] = 'mongodb://localhost:27017/projet1'
mongo = PyMongo(app)

@app.route("/")
def hello():
    collection = mongo.db.uni
    documents = collection.find({})
    # return render_template("index.html")
    return "documents"

if __name__=="__main__":
    app.run(debug=True)